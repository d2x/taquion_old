package org.apache.mesos.chronos.notification

import java.util.logging.Logger

import org.apache.mesos.chronos.scheduler.jobs.BaseJob

import scalaj.http._

/**
  * A very simple mail client that works with Pombo Correio
  *
  * @author André Claudino (andre.claudino@stone.com.br)
  */
class SlackStoneClient(val url: String, val channel: String) extends NotificationClient {

  private[this] val log = Logger.getLogger(getClass.getName)

  def sendNotification(job: BaseJob, to: String, subject: String, message: Option[String]) {
    val complete_message = s"```${subject.toString} ${if(message.isEmpty) "" else message.toString}```".replace('\n', ' ')

    if(subject.isEmpty && message.isEmpty){
      log.info("Slack Stone notification not sent because there is no recipient")
      return
    }

    val payload = s"""
                     |{
                     |	"ChannelName": "${channel}",
                     |	"BotUsername": "Taquion Bot",
                     |	"Message": "${complete_message}",
                     |	"IconEmoji": ":atom_symbol:"
                     |}
                  """.stripMargin
    val response =
          Http(url)
            .postData(payload)
            .header("content-type", "application/json")
            .asString

    if(response.code == 200){
      log.info("Sent slack to '%s': '%s', got response code '%s': %s".format(channel, response.code, response.body))
    } else {
      log.severe("Erro sending slack to '%s': '%s', got response code '%s': %s".format(channel, response.code, response.body))
    }

  }

}
