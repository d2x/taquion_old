package org.apache.mesos.chronos.notification

import java.util.logging.Logger
import scalaj.http._
import org.apache.mesos.chronos.scheduler.jobs._
import org.joda.time.{DateTime, DateTimeZone}

/**
  * A very simple mail client that works with Pombo Correio
  *
  * @author André Claudino (andre.claudino@stone.com.br)
  */
class PomboClient(
                  val url: String,
                  val authHeader: String,
                  val from: String,
                  val senderName: String) extends NotificationClient {

  private[this] val log = Logger.getLogger(getClass.getName)
  override def notHandledEvents
              = List(classOf[JobQueued],
                     classOf[JobStarted],
                     classOf[JobFinished])

  def sendNotification(job: BaseJob, to: String, subject: String, message: Option[String]) {
    if(to.isEmpty){
      log.info("Mail notification not sent because there is no recipient")
      return
    }

    val payload = s"""
                     |{
                     |	"company": "Stone",
                     |    "costCenter": "$senderName",
                     |    "messages": [{
                     |    	"from":{
                     |    		"address": "$from",
                     |    		"name": "$senderName"
                     |    	},
                     |    	"to":[{
                     |    		"subscriberKey": "$to",
                     |    		"address": "$to"
                     |    	}],
                     |    	"sendStructure": 1,
                     |    	"subject": "${subject.toString}",
                     |    	"body": "Message sent at ${DateTime.now(DateTimeZone.UTC)}: ${if(message.isEmpty) "" else message.toString.replace('\n', ' ')}"
                     |    }]
                     |
                     |}
                  """.stripMargin
    val response =
          Http(url)
            .postData(payload)
            .header("content-type", "application/json")
            .header("Authorization", authHeader)
            .asString

    if(response.code == 202){
      log.info("Sent e-mail to '%s' with subject: '%s', got response code '%s': %s".format(to, subject, response.code, response.body))
    } else {
      log.severe("Error sending e-mail to '%s' with subject: '%s', got response code '%s': %s".format(to, subject, response.code, response.body))
    }

  }

}
