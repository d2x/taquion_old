package org.apache.mesos.chronos.notification

import java.util.logging.Logger

import akka.actor.ActorRef
import com.google.inject.Inject
import org.apache.mesos.chronos.scheduler.jobs._
import org.joda.time.{DateTime, DateTimeZone}

class JobNotificationObserver @Inject()(val notificationClients: List[ActorRef] = List(),
                                        val clusterName: Option[String] = None) {
  val clusterPrefix = clusterName.map(name => s"[$name]").getOrElse("")
  private[this] val log = Logger.getLogger(getClass.getName)

  def asObserver: JobsObserver.Observer = JobsObserver.withName({

    case JobQueued(job, taskId, attempt) =>
      val msg = s"A tarefa $taskId do job ${job.name} (tentativa $attempt) foi enfileirada para execuaap;"
      sendNotification( job,
          s"$clusterPrefix [Taquion] job '${job.name}' enfileirado",
        None, Some(classOf[JobQueued]))

    case JobRemoved(job) =>
      val msg = s"O job ${job.name} foi apagado"
      sendNotification(job, s"$clusterPrefix [Taquion] job '${job.name}' apagado", Some(msg),
        Some(classOf[JobRemoved]))

    case JobStarted(job, taskStatus, attempt, runningCount) =>
      val msg = s"O job ${job.name} iniciou a tentativa $attempt de ${job.retries} sendo executado $runningCount"
      sendNotification(job, s"$clusterPrefix [Taquion] job '${job.name}' iniciado", Some(msg),
        Some(classOf[JobStarted]))

    case JobFinished(job, taskStatus, attempt, runningCount) =>
      sendNotification(job, s"$clusterPrefix [Taquion] job '${job.name}' finalizado", None,
        Some(classOf[JobStarted]))

    case JobDisabled(job, cause) =>
      sendNotification( job, s"$clusterPrefix [Taquion] job '${job.name}' desativado", Some(cause),
        Some(classOf[JobDisabled]))

    case JobExpired(job, taskId) =>
      val msg = s"A task $taskId do job ${job.name} foi expirada"
      sendNotification( job, s"$clusterPrefix [Taquion] job '${job.name}' expirado", Some(msg),
        Some(classOf[JobDisabled]))

    case JobRetriesExhausted(job, taskStatus, attempts) =>
      val msg =
        s"'${DateTime.now(DateTimeZone.UTC)}'. Após ${job.retries} tentativas o job ${job.name} " +
          s"falhou na tarefa de id ${taskStatus.getTaskId.getValue}"
      sendNotification(job, s"$clusterPrefix [Taquion] job '${job.name}' falhou!",
        Some(TaskUtils.appendSchedulerMessage(msg, taskStatus)),
        Some(classOf[JobRetriesExhausted]))

    case JobSkipped(job, dateTime) =>
      val msg = s"Job ${job.name} foi ignorado às ${dateTime}"
      sendNotification(job, s"${clusterPrefix} [Taquion] job ${job.name} ignorado!", Option(msg),
        Some(classOf[JobSkipped]))

    case JobFailed(job, taskStatus, attempt, runningCount) =>
      job match {
        case Right(j) =>
          val msg = s"Job ${j.name} falhou na tentativa $runningCount de $attempt.: ${taskStatus.getMessage}"
          sendNotification(j, s"$clusterPrefix [Taquion] job ${j.name} falhou!",
            Some(TaskUtils.appendSchedulerMessage(msg, taskStatus)),
            Some(classOf[JobFailed]))
        case _ =>

      }

  }, getClass.getSimpleName)

  def sendNotification(job: BaseJob, subject: String, message: Option[String] = None, event:Option[Class[_]] = None) {
    for (client <- notificationClients) {
      val subowners = job.owner.split("\\s*,\\s*")
      for (subowner <- subowners) {
        log.info("Sending notification to:%s for job %s using client: %s".format(subowner, job.name, client))
        if(event.isEmpty)
          client ! (job, subowner, subject, message)
        else
          client ! (job, subowner, subject, message, event)
      }
    }

    log.info(subject)
  }

}
